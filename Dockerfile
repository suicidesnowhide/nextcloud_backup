FROM debian:latest

RUN mkdir /scripts

ADD backup.sh /scripts/backup.sh

RUN chmod a+x /scripts/*.sh

CMD /scripts/backup.sh