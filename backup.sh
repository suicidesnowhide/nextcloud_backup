#!/usr/bin/env bash
apt update

apt-get install -y rclone curl

curl -L -o rustic.tar.gz https://github.com/rustic-rs/rustic/releases/download/v0.3.1/rustic-v0.3.1-x86_64-unknown-linux-gnu.tar.gz
tar xfz rustic.tar.gz -C /usr/local/bin/
rustic self-update

rclone config create nextcloud webdav url https://oc.th-ht.de/remote.php/dav/files/thht_manjaro_repo/ vendor nextcloud user thht_manjaro_repo pass $RCLONEPASS

mkdir /mnt/nextcloud

rclone mount nextcloud:/ /mnt/nextcloud

ls /mnt/nextcloud